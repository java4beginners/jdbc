package ua.com.java4beginners;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

import java.util.Properties;

public class App {
    public static void main( String[] args ) {
        SessionFactory sessionFactory = new Configuration()
                .setProperties(getHibernateProperties())
                .addAnnotatedClass(User.class)
                .buildSessionFactory();

        // start
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        User vova = new User("Vova", "john@gmail.com", "123", "USER");
        session.persist(vova);

        transaction.commit();
        session.close();
        //finish


        sessionFactory.close();
        System.out.println(vova);


    }

    private static Properties getHibernateProperties() {
        Properties props = new Properties();
        props.put(Environment.DRIVER, "com.mysql.cj.jdbc.Driver");
        props.put(Environment.URL, "jdbc:mysql://127.0.0.1:3306/crm?useSSL=false");
        props.put(Environment.USER, "root");
        props.put(Environment.PASS, "root");
        props.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
        props.put(Environment.SHOW_SQL, "true");
        return props;
    }

}
