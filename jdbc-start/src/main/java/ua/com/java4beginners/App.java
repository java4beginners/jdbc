package ua.com.java4beginners;

import java.sql.*;

public class App {
    private static final String DB_URL = "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1";
    private static final String DB_USER = "";
    private static final String DB_PASSWORD = "";
    private static final String DB_DRIVER = "org.h2.Driver";

    public static void main(String[] args) throws SQLException, ClassNotFoundException {

        Class.forName(DB_DRIVER);
        Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

        Statement stmt = connection.createStatement();
        stmt.execute("create table products( title varchar(255), price int )");
        stmt.execute("insert into products values('book', 125)");
        stmt.execute("insert into products values('notebook', 2430)");
        stmt.execute("insert into products values('pen', 11)");

        ResultSet res = stmt.executeQuery("select title, price from products");
        while(res.next()) {
            Integer price = res.getInt("price");
            String title = res.getString("title");
            System.out.printf("%15s|%5d\n", title, price);
        }

        stmt.close();
        connection.close();
    }
}
